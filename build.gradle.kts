plugins {
    java
    id("org.springframework.boot") version "3.3.3"
    id("io.spring.dependency-management") version "1.1.6"
}

group = "cn.apelx"
version = "0.0.1-SNAPSHOT"

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}


repositories {
    maven { setUrl("https://mirrors.huaweicloud.com/repository/maven/") }
    maven { setUrl("https://mirrors.tencent.com/nexus/repository/maven-public/") }
    maven { setUrl("https://maven.aliyun.com/nexus/content/groups/public/") }
    maven { setUrl("https://build.shibboleth.net/nexus/content/repositories/releases/") } // for saml
}

dependencies {
    implementation("com.ctrip.framework.apollo:apollo-client:2.3.0")

    implementation("org.springframework.boot:spring-boot-starter-web")
    compileOnly("org.projectlombok:lombok")
    annotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.jar {
    enabled = true
    manifest {
        attributes(mapOf("Main-Class" to "cn.apelx.apolloapi.ApolloApiApplication"))
    }
}