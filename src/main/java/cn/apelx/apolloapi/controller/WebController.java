package cn.apelx.apolloapi.controller;

import cn.apelx.apolloapi.apollo.ApolloHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {

    @Value("${name}")
    private String name;

    @GetMapping("/name")
    public String test() {
        return name;
    }


    @Value("${tk}")
    private String tk;

    @GetMapping("/tk")
    public String tk() {
        return tk;
    }

    @Value("${redis.url}")
    private String redisUrl;

    @GetMapping("/redis/url")
    public String redisUrl() {
        return redisUrl;
    }

    @GetMapping("/redis/url/2")
    public String redisUrl2() {
        return ApolloHelper.getConfig("common.redis", "redis.url", "not exist!");
    }
}
