package cn.apelx.apolloapi;

import cn.apelx.apolloapi.apollo.ApolloHelper;
import com.ctrip.framework.apollo.model.ConfigChange;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

@EnableApolloConfig
@SpringBootApplication
public class ApolloApiApplication {

    public static void main(String[] args) {

        SpringApplication.run(ApolloApiApplication.class, args);


        ApolloHelper.addConfigListener("name", new Consumer<ConfigChange>() {
            @Override
            public void accept(ConfigChange configChange) {
                System.err.printf("name发生变化!  变化类型: %s, oldValue: %s, newValue: %s %n", configChange.getChangeType().name(), configChange.getOldValue(), configChange.getNewValue());
            }
        });


        ApolloHelper.addConfigListener("ns0", new BiConsumer<String, ConfigChange>() {
            @Override
            public void accept(String key, ConfigChange configChange) {
                System.err.printf("命名空间: ns0 下key: %s 发生变化! 变化类型: %s, oldValue: %s, newValue: %s %n", key, configChange.getChangeType().name(), configChange.getOldValue(), configChange.getNewValue());
            }
        });


        ApolloHelper.addConfigListener("common.redis", new BiConsumer<String, ConfigChange>() {
            @Override
            public void accept(String key, ConfigChange configChange) {
                System.err.printf("命名空间: common.redis 下key: %s 发生变化! 变化类型: %s, oldValue: %s, newValue: %s %n", key, configChange.getChangeType().name(), configChange.getOldValue(), configChange.getNewValue());
            }
        });
    }

}
