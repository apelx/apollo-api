package cn.apelx.apolloapi.apollo;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.ctrip.framework.apollo.model.ConfigChange;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

@Component
public class ApolloHelper {

    public static String getConfig(String key) {
        return getConfig(null, key, null);
    }

    public static String getConfig(String key, String defaultValue) {
        return getConfig(null, key, defaultValue);
    }

    public static String getConfig(String namespace, String key, String defaultValue) {
        Config appConfig = getAppConfig(namespace);
        return appConfig.getProperty(key, defaultValue);
    }

    public static <T> T getConfig(String key, Function<String, T> valFunction) {
        return getConfig(null, key, valFunction, null);
    }

    public static <T> T getConfig(String key, Function<String, T> valFunction, T defaultValue) {
        return getConfig(null, key, valFunction, defaultValue);
    }

    public static <T> T getConfig(String namespace, String key, Function<String, T> valFunction, T defaultValue) {
        return Optional.ofNullable(getConfig(namespace, key))
                .map(valFunction)
                .orElse(defaultValue);
    }


    public static void addConfigListener(String namespace, String key, Consumer<ConfigChange> consumer) {
        Config appConfig = getAppConfig(namespace);
        appConfig.addChangeListener(changeEvent -> {
            ConfigChange change = changeEvent.getChange(key);
            consumer.accept(change);
        }, Set.of(key));
    }

    public static void addConfigListener(String key, Consumer<ConfigChange> consumer) {
        addConfigListener(null, key, consumer);
    }


    /**
     * namespace 维度key变化
     */
    public static void addConfigListener(String namespace, BiConsumer<String, ConfigChange> consumer) {
        Config appConfig = getAppConfig(namespace);
        appConfig.addChangeListener(changeEvent -> changeEvent.changedKeys().forEach(key -> consumer.accept(key, changeEvent.getChange(key))));
    }


    private static Config getAppConfig(@Nullable String namespace) {
        return Optional.ofNullable(namespace).map(ConfigService::getConfig).orElseGet(ConfigService::getAppConfig);
    }
}
